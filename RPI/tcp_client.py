import numpy as np
import cv2
from pylepton import Lepton
from time import sleep
import socket
from subprocess import call, check_output

a_avg = 0
a_min = 0
a_max = 0
def capture(device = "/dev/spidev0.0"):
    global a_avg,a_min,a_max
    with Lepton() as l:
      a,_ = l.capture()     #grab the buffer
    a_offset = np.load('pixel_offsets.npy')
    a = a - a_offset
    a_avg = np.uint16(np.sum(a)/4800)
    a_min = np.uint16(np.min(a))
    a_max = np.uint16(np.max(a))
    #print a_avg, a_min, a_max
    cv2.normalize(a, a, 0, 65535, cv2.NORM_MINMAX) # extend  contrast
    np.right_shift(a, 8, a) # fit data into 8 bits
    
    
    return a


call('./configure-lepton')
result = check_output('./configure-lepton',shell=True)
if (result == '1'):
	TCP_IP = '192.168.1.104'
	TCP_PORT = 5005
	BUFFER_SIZE = 9192
	while 1:
	    
	    frame = capture()
	    
	    #frame = frame.reshape((60,80))
	    #frame = np.ones((60,80))
	    MESSAGE = np.array(frame,dtype = np.uint8)
            #MESSAGE[59,80] = frame_avg
	    #print MESSAGE
	    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	    s.connect((TCP_IP, TCP_PORT))
	    err = s.send(MESSAGE)
	    print err
	    err2 = s.send(a_avg)
	    print err2
	    err3 = s.send(a_min)
	    print err3
	    err4 = s.send(a_max)
	    print err4
	    dataAll = ""
	    while 1:
	        data = s.recv(BUFFER_SIZE)
	        dataAll = dataAll + data
	        if (len(dataAll) == err+err2+err3+err4): break
	    print len(dataAll)
	    s.close()
	    sleep(0.05)
	
	#dataR = np.frombuffer(data,dtype = np.uint8)
	#print "received data:", dataR, np.shape(dataR)

