#!/usr/bin/env python
'''image_saver ROS Node'''
# license removed for brevity
import rospy
from std_msgs.msg import String

def talker():
    '''image_saver Publisher'''
    pub = rospy.Publisher('save_images', String, queue_size=10)
    rospy.init_node('image_saver_talker', anonymous=True)
    pub.publish("1")
        

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
