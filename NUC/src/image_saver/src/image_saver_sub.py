#!/usr/bin/env python
'''image_saver ROS Node'''
import rospy
import message_filters
from std_msgs.msg import String
from sensor_msgs.msg import Image
import cv2
import numpy as np
from cv_bridge import CvBridge, CvBridgeError

#global save1
#global save2
save1 = 0
save2 = 0
def callback0(data):
	global save1, save2
	save1 = 1
	save2 = 1

def callback1(image):
    '''image_saver Callback Function'''
    global save1
    if (save1 ==1):
		rospy.loginfo(rospy.get_caller_id() + "I heard %s", image.header.stamp)
		bridge = CvBridge()
		try:
		    img = bridge.imgmsg_to_cv2(image, "passthrough")
		except CvBridgeError as e:
		    print(e)
		name = str(image.header.stamp)
		e = cv2.imwrite("/home/robot/saved_images/camera_"+ name +".png",img)
		print e
		save1 = 0
    
def callback2(image):
    '''image_saver Callback Function'''
    global save2 
    if (save2 ==1):
        rospy.loginfo(rospy.get_caller_id() + "I heard %s", image.header.stamp)
        bridge = CvBridge()
        try:
            img = bridge.imgmsg_to_cv2(image, "passthrough")
        except CvBridgeError as e:
            print(e)
        name = str(rospy.Time.now())
        #name = str(image.header.stamp)
        cv2.imwrite("/home/robot/saved_images/termo_camera_"+ name +".png",img)
        save2 = 0


def listener():
    '''image_saver Subscriber'''
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('image_saver', anonymous=True)
	
    rospy.Subscriber("/save_images", String, callback0)
    rospy.Subscriber("/cv_camera/image_raw", Image, callback1)
    rospy.Subscriber("/termo_image",Image, callback2)
	
   

   

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    listener()
