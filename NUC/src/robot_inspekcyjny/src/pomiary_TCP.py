#!/usr/bin/env python

import socket
import sys
import rospy
import Queue
import math
import copy
import time
import numpy as np

from visualization_msgs.msg import Marker, MarkerArray

from move_base_msgs.msg import MoveBaseActionResult
from geometry_msgs.msg import PoseStamped
from tf.transformations import quaternion_from_euler, euler_from_quaternion




class RemoteMeasurments():

    TCP_IP = '192.168.50.237'
    TCP_PORT = 5006
    BUFFER_SIZE = 1024  # Normally 1024, but we want fast response
    Points = []
    s = None
    conn = None
    addr = None
    RotationAngle = 45
    RotationAngleRad = RotationAngle*math.pi/180
    Translation = [0,0]
    TransformMatrix = [ [ math.cos(RotationAngleRad), math.sin(RotationAngleRad), Translation[0]],
                        [-math.sin(RotationAngleRad), math.cos(RotationAngleRad), Translation[1]],
                        [0,                       0,                              1             ]]

    GoalTopic = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)
    MarkersTopic = rospy.Publisher('/patrol_points',MarkerArray, queue_size=10)

    PatrolPoints = Queue.Queue()
    markerArray = MarkerArray()
    Patrolling = 0

    def __init__(self):
        self.markerArray.markers = []

    def WaitForClient(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind((self.TCP_IP, self.TCP_PORT))
        print("waitnig for connection")
        self.s.listen(1)
        self.conn, self.addr = self.s.accept()
        print 'Connection address:', self.addr
        self.running = True

    def WaitForPointsFromClient(self):
        ReceivedData = ""
        while 1:
            data = self.conn.recv(self.BUFFER_SIZE)
            if not data: 
                data = ""
            ReceivedData = ReceivedData+data
            ReceivedData = str(ReceivedData)
            if "END_OF_FILE" in ReceivedData:
                self.Points = ReceivedData.split("\n")
                self.Points = self.Points[0:-1]
                break            
        pointsTemp = []
	print(self.Points)
        for i,(p) in enumerate(self.Points):
                if p[-1] == "\r":
                    p = p[0:-1]
                p = p.split(',')
                if len(p) == 3:
                    if i == 0:
                        self.Translation[0] = int(p[0])/1000.0
                        self.Translation[1] = int(p[1])/1000.0
                        self.RotationAngle = int(p[2])
                        self.RotationAngleRad = int(p[2])*math.pi/180
                        self.CalculateTransformMatrix()
                        continue
                    pointsTemp.append([int(p[0])/1000.0,int(p[1])/1000.0,int(p[2])+self.RotationAngle])
                
        self.Points = pointsTemp
        self.SendCommunicate("Points Loaded")  # echo
    
    def SendCommunicate(self,communicate):
        try:
            self.conn.send("#")
            self.conn.send(communicate)
            self.conn.send("*")
        except Exception:
            # raise KeyboardInterrupt
            self.running = False
            print(Exception)

    def ReceivedChar(self,char):
        try:
            ReceivedChar = ""
            while 1:
                ReceivedChar = self.conn.recv(1)
                if ReceivedChar == char: 
                    break
                elif ReceivedChar == '':
                    raise Exception
        except Exception:
            # raise rospy.ROSInterruptException()
            self.running = False
            print(Exception)   

    def GetConfirmation(self,message):
        self.SendCommunicate(message)
        self.ReceivedChar("G")
   
    def closeConnection(self):
        self.conn.close()
        self.s.close()
 
    def CalculateTransformMatrix(self):
        self.TransformMatrix = [ [ math.cos(self.RotationAngleRad), math.sin(self.RotationAngleRad), self.Translation[0]],
                                [-math.sin(self.RotationAngleRad), math.cos(self.RotationAngleRad), self.Translation[1]],
                                [0,                       0,                              1             ]]


    def TransformPoints(self):
        for i,(p) in enumerate(self.Points):
            point=[p[0],p[1],1]
            point = np.matmul(self.TransformMatrix,point)
            self.Points[i] = [point[0],point[1],p[2]]
        
    def AddPointToMarkerArray(self,Point):
        marker = Marker()
        marker.header.frame_id = Point.header.frame_id
        marker.type = marker.ARROW
        marker.action = marker.ADD
        marker.scale.x = 0.5
        marker.scale.y = 0.05
        marker.scale.z = 0.05
        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.pose = Point.pose
        # marker.pose.position.x = Point.pose.position.x
        # marker.pose.position.y = Point.pose.position.y
        # marker.pose.position.z = Point.pose.position.z
        
        self.markerArray.markers.append(marker)

        id = 0
        for m in self.markerArray.markers:
            m.id = id
            id += 1
        # Publish the MarkerArray
        self.MarkersTopic.publish(self.markerArray)

    def GoToNextPoint(self):
        if not self.PatrolPoints.empty():
            point = self.PatrolPoints.get()
            rospy.loginfo("Next  point x: %f, y:%f",point.pose.position.x,point.pose.position.y)
            self.SendCommunicate("Next  point x: "+str(point.pose.position.x)+", y:"+str(point.pose.position.y))
            self.GoalTopic.publish(point)
        else:
            self.EndPatrol()

    def addPointsToPatrolPoints(self):
        for x,y,yaw in self.Points:
            Pose = PoseStamped()
            Pose.header.frame_id = 'map'
            Pose.pose.position.x = x
            Pose.pose.position.y = y
            rospy.loginfo("point x: %f, y:%f, yaw:%f",x,y,yaw)
            yaw = (yaw*math.pi)/180
            
            q = quaternion_from_euler(0, 0, yaw)
            Pose.pose.orientation.x = q[0]
            Pose.pose.orientation.y = q[1]
            Pose.pose.orientation.z = q[2]
            Pose.pose.orientation.w = q[3]
            self.PatrolPoints.put(Pose)
            self.AddPointToMarkerArray(Pose)

    def StartMeasurements(self):
        if not self.PatrolPoints.empty():
            self.Patrolling = 1
            self.GoToNextPoint()

    def EndPatrol(self):
        self.Patrolling = 0
        rospy.loginfo("Measurements have been completed")
        self.SendCommunicate("Measurements have been completed")
        self.running = False

    def DoMeasurements(self):
        self.GetConfirmation("\nDo you want to go to next point?")
        # TUTAJ JAKIS STOP................
        rospy.loginfo("Measurement done")

    def RobotResultcallback(self,data):
        if data.status.text == "Goal reached.":
            rospy.loginfo("Goal reached.")
            self.SendCommunicate("Goal reached.")
            self.DoMeasurements()
            self.GoToNextPoint()

    

    def listener(self):
        rospy.loginfo("Start")
        rospy.init_node('Measurements', anonymous=True)    
        rospy.Subscriber("move_base/result", MoveBaseActionResult, self.RobotResultcallback)

        self.MarkersTopic.publish(self.markerArray)
        print("Waiting for client")
        self.WaitForPointsFromClient()
        self.TransformPoints()
        self.addPointsToPatrolPoints()
        self.SendCommunicate("Points Initialized")
        self.GetConfirmation("Do you want to start measurements?")
        if self.running:
            self.StartMeasurements()
        while self.running:
            time.sleep(1)
        print("Measurements interrupted")


if __name__ == '__main__':
    while True:
        rm = RemoteMeasurments()
        try:
            rm.WaitForClient()
            rm.listener()
            rm.closeConnection()
            print("connection closed")
            time.sleep(1)
        except KeyboardInterrupt:
            rm.closeConnection()
            print("Keyboard Interrupt")
            break
        
