
import socket
import apa102
import threading
import mraa

import time
from voice_engine.source import Source
from voice_engine.channel_picker import ChannelPicker
from voice_engine.kws import KWS
from voice_engine.ns import NS
from voice_engine.doa_respeaker_6p1_mic_array import DOA

pixels_switch = mraa.Gpio(12)
time.sleep(1)
pixels_switch.dir(mraa.DIR_OUT)
pixels_switch.write(0)

class Pixels:
    PIXELS_N = 12

    def __init__(self):
        self.dev = apa102.APA102(num_led=self.PIXELS_N)

    def detected(self, led):
        #led = int(round(direction/(360/self.PIXELS_N))) + 11
        for i in range(0,self.PIXELS_N):
            self.dev.set_pixel(i,40,40,40,65)
        self.dev.set_pixel((led-1)%12,0,50,0,65)          
        self.dev.set_pixel(led%12,0,50,0,65)           
        self.dev.set_pixel((led+1)%12,0,50,0,65)     
        self.dev.show()

    def listen(self):
        for i in range(0,self.PIXELS_N):
            self.dev.set_pixel(i,0,0,50,65)
        self.dev.show()

    def wait_kws(self):
        for i in range(0,self.PIXELS_N):
            self.dev.set_pixel(i,50,0,0,65)
        self.dev.show()

    def end(self):
        for i in range(0,self.PIXELS_N):
            self.dev.set_pixel(i,1,1,1)
        self.dev.show()       

def dominant(A, r):
    W = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    count = 0
    for i in range(r):
        for j in range(r):
            if A[i] == A[j]:
                count = count + 1
            else:
                continue
        W[i] = count
        count = 0 
    x = A[i]
    for i in range(r-1):        
        if W[i+1] >= W[i]:
            x = A[i+1]
        else:
            W[i+1] = W[i]
    return (x)


def main():
    pixels = Pixels()

    ####
    TCP_IP = '192.168.1.103'
    #TCP_IP = '127.0.0.1'
    TCP_PORT = 5005
    BUFFER_SIZE = 1024
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while True:
        try:
            s.connect((TCP_IP, TCP_PORT))
            print 'Connected'
            break      
        except:
            print 'Failed to connect, will try again in sec'
            time.sleep(1)
    ####

    src = Source(rate=16000, frames_size=320, channels=8)
    ch1 = ChannelPicker(channels=8, pick=1)
    ns = NS(rate=16000, channels=1)
    kws = KWS()
    doa = DOA(rate=16000, chunks=20)

    src.link(ch1)
    ch1.link(ns)
    ns.link(kws)
    src.link(doa)

    pixels.wait_kws()
    Samples = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]

    def on_detected(keyword):
        time.sleep(0.4)
        while(1):
            pixels.listen()
            time.sleep(0.4)
            for i in range(20):
                direction = doa.get_direction()
                print('detected {} at direction {}'.format(keyword, direction))           
                Samples[i] = int(round(direction/(360/12))) + 11  
                time.sleep(0.04)      
            led_arrow = dominant(Samples, len(Samples)) 
            print('Ostatecznym wynikiem dominanty          jest {}'.format(led_arrow))                
            pixels.detected(led_arrow)
            time.sleep(3)
            #####
            
            MESSAGE = str(((led_arrow%12 + 1) * 30))
            s.send(MESSAGE)
            data = s.recv(BUFFER_SIZE)
            print " ", data
            time.sleep(3)
            
            #####
    kws.set_callback(on_detected)
    src.recursive_start()
    

    while True:
        try:
            time.sleep(1)
        except KeyboardInterrupt:
            break   
      
    src.recursive_stop()
    pixels.end()
    time.sleep(1)

if __name__ == '__main__':
    main()
