#!/usr/bin/env python

import sys
import rospy
import Queue
import math
import copy
import time
import numpy as np

from visualization_msgs.msg import Marker, MarkerArray
from std_msgs.msg import String
from move_base_msgs.msg import MoveBaseActionResult
from geometry_msgs.msg import PoseStamped
from tf.transformations import quaternion_from_euler, euler_from_quaternion

GoalTopic = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)
MarkersTopic = rospy.Publisher('/patrol_points',MarkerArray, queue_size=10)
SaveImagesTopic = rospy.Publisher('/save_images',String, queue_size=10)

PatrolPoints = Queue.Queue()
markerArray = MarkerArray()
Patrolling = 0
directory = '~/catkin_ws/src/robot_inspekcyjny/src/points.txt'

RotationAngle = 150
RotationAngleRad = RotationAngle*math.pi/180
Translation = [4.5,3]
TransformMatrix = [ [ math.cos(RotationAngleRad), math.sin(RotationAngleRad), Translation[0]],
                    [-math.sin(RotationAngleRad), math.cos(RotationAngleRad), Translation[1]],
                    [0,                       0,                              1             ]]


def readFile(directory):
    F = open("points.txt", "r")
    Points = []

    for line in F:
        Points.append(line.split(","))

    for i,(x,y,yaw) in enumerate(Points):
        Point=[int(x)/1000.0,int(y)/1000.0,1]
        Point = np.matmul(TransformMatrix,Point)
        Points[i] = [Point[0],Point[1],int(yaw)+RotationAngle]
    return Points

def AddPointToMarkerArray(Point):
    marker = Marker()
    marker.header.frame_id = Point.header.frame_id
    marker.type = marker.ARROW
    marker.action = marker.ADD
    marker.scale.x = 0.5
    marker.scale.y = 0.05
    marker.scale.z = 0.05
    marker.color.a = 1.0
    marker.color.r = 0.0
    marker.color.g = 1.0
    marker.color.b = 0.0
    marker.pose = Point.pose
    # marker.pose.position.x = Point.pose.position.x
    # marker.pose.position.y = Point.pose.position.y
    # marker.pose.position.z = Point.pose.position.z
    
    markerArray.markers.append(marker)

    id = 0
    for m in markerArray.markers:
        m.id = id
        id += 1
    # Publish the MarkerArray
    MarkersTopic.publish(markerArray)

def GoToNextPoint():
    if not PatrolPoints.empty():
        point = PatrolPoints.get()
        rospy.loginfo("Next  point x: %f, y:%f",point.pose.position.x,point.pose.position.y)
        GoalTopic.publish(point)
    else:
        EndPatrol()

def addPointsToPatrolPoints(Points):
    for x,y,yaw in Points:
        Pose = PoseStamped()
        Pose.header.frame_id = 'map'
        Pose.pose.position.x = x
        Pose.pose.position.y = y
        rospy.loginfo("point x: %f, y:%f, yaw:%f",x,y,yaw)
        yaw = (yaw*math.pi)/180
        
        q = quaternion_from_euler(0, 0, yaw)
        Pose.pose.orientation.x = q[0]
        Pose.pose.orientation.y = q[1]
        Pose.pose.orientation.z = q[2]
        Pose.pose.orientation.w = q[3]
        PatrolPoints.put(Pose)
        AddPointToMarkerArray(Pose)

def StartMeasurements():
    if not PatrolPoints.empty():
        Patrolling = 1
        GoToNextPoint()

def EndPatrol():
    Patrolling = 0
    rospy.loginfo("Measurements have been completed")

def DoMeasurements():
     rospy.loginfo("Measurement done")
     # TUTAJ JAKIS STOP................
     msg = String()
     #SaveImagesTopic.publish(msg)
     time.sleep(10)
     #raw_input("Press key: ")

def RobotResultcallback(data):
    if data.status.text == "Goal reached.":
        rospy.loginfo("Goal reached. :)")
        DoMeasurements()
        GoToNextPoint()

def listener():
    rospy.loginfo("Start")
    rospy.init_node('Measurements', anonymous=True)    
    rospy.Subscriber("move_base/result", MoveBaseActionResult, RobotResultcallback)
    # rospy.Subscriber("clicked_point", PointStamped, PointClickCallback)
    # rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, PoseCallback)
    # spin() simply keeps python from exiting until this node is stopped
    # rospy.spin()
    MarkersTopic.publish(markerArray)
    raw_input("Read file:")
    addPointsToPatrolPoints(readFile(directory))
    raw_input("Start measurements!!:")
    StartMeasurements()
    rospy.spin()

if __name__ == '__main__':
    listener()
