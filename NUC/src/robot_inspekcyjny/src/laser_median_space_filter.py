#!/usr/bin/env python
'''laser_median_space_filter.py ROS Node'''
import rospy
from sensor_msgs.msg import LaserScan
from mediana import median_space_filter

pub = rospy.Publisher('/scan_filtred', LaserScan, queue_size=10)
filterWindow = (int)(rospy.get_param('/laser_median_space_filter/laser_median_space_filter_window', '20'))
def callback(data):
    '''laser_median_space_filter.py Callback Function'''
    msg = data
    msg.ranges = median_space_filter(filterWindow, data.ranges)
    # rospy.loginfo(rospy.get_caller_id() + "I heard %s", data.data)
    pub.publish(msg)

def laser_median_space_filter():
    '''laser_median_space_filter.py Subscriber'''
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('laser_median_space_filter.py', anonymous=False)

    rospy.Subscriber("/scan", LaserScan, callback)
    rospy.loginfo("Filter started")
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    try:
        laser_median_space_filter()
    except rospy.ROSInterruptException:
        pass
