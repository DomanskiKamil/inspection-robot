import time
import sys

def median(lst):
    even = (0 if len(lst) % 2 else 1) + 1
    half = (len(lst) - 1) / 2
    medianVal = sorted(lst)[half:half + even]
    try:
        val = sum(medianVal) / float(even)
        return val
    except Exception as inst:
        print(type(inst))   
        print(inst.args)
        print(inst)
        sys.stdout.flush()
        return None

def median_space_filter(window, data):
    dataLength = len(data)
    halfofWindow = (int)(window/2)
    filtredData = []
    for i in range(dataLength):
        lista=[]
        if(i-halfofWindow<0):
            lista = data[i-halfofWindow:] + data[:i]
        else:
            lista = data[i-halfofWindow:i]
        lista = lista + data[i:i+halfofWindow+1]
        if(i+halfofWindow>=dataLength):
            lista = lista + data[:(i+halfofWindow-dataLength+1)]
        filtredData.append(median(lista))
    return filtredData
