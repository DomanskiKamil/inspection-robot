#!/usr/bin/env python
'''xbox_gamepad_velocyty_control ROS Node'''
import sys
import getopt
import rospy

from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist

pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
velocity = 4
offset = 0.3
MaxForwordSpeed = 0.5
MaxAngularSpeed = 4.25
msg = Twist()
msg.angular.z = 0
msg.linear.x = 0
def callback(data):
    '''xbox_gamepad_velocyty_control Callback Function'''
    x = data.axes[2]
    Xabs = abs(x)
    y = data.axes[1]
    Yabs = abs(y)
    if Xabs > offset:
        msg.angular.z = x/Xabs*(Xabs-offset)/(1-offset)*MaxAngularSpeed
    else:
        msg.angular.z = 0
    if Yabs > offset:
        msg.linear.x = y/Yabs*(Yabs-offset)/(1-offset)*MaxForwordSpeed
    else:
        msg.linear.x = 0
    pub.publish(msg)
    rospy.loginfo(rospy.get_caller_id() + "I heard %f", data.axes[0])

def listener(argv):
    '''xbox_gamepad_velocyty_control Subscriber'''
    try:
        opts, args = getopt.getopt(argv,"v:",["velocity="])
    except getopt.GetoptError:
        print 'test.py -v <velocity>'
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -v <velocity> '
            sys.exit()
        elif opt in ("-v"):
            if arg > 0:
                MaxForwordSpeed = arg
            else:
                print 'Wrong velocity'
                return
    rospy.init_node('xbox_gamepad_velocyty_control', anonymous=True)    
    rospy.Subscriber("joy", Joy, callback)
    rate = rospy.Rate(20) # 10hz
    while not rospy.is_shutdown():
        pub.publish(msg)
        rate.sleep()
    # spin() simply keeps python from exiting until this node is stopped
    # rospy.spin()

if __name__ == '__main__':
    listener(sys.argv[1:])
