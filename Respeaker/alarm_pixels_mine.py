
import audioop
import os
import signal
import socket

from avs.mic import Audio
from avs.player import Player

import apa102
import time
import threading
import mraa

pixels_switch = mraa.Gpio(12)
time.sleep(1)
pixels_switch.dir(mraa.DIR_OUT)
pixels_switch.write(0)

BUFFER_SIZE = 1024
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

class Pixels:
    PIXELS_N = 12

    def __init__(self):
        self.dev = apa102.APA102(num_led=self.PIXELS_N)

    def cross(self):
        self.dev.set_pixel(2,25,0,0,65)
        self.dev.set_pixel(5,0,25,0,65)
        self.dev.set_pixel(8,0,0,25,65)
        self.dev.set_pixel(11,25,25,25,65)
        self.dev.show()

    def alarm(self):
        for i in range(self.PIXELS_N):
            if (i%2 == 0):
                self.dev.set_pixel(i,50,0,0,65)
            else:
                self.dev.set_pixel(i,0,0,50,65) 
        self.dev.show()
        time.sleep(0.2)
        self.dev.rotate(1)
        self.dev.show()
        time.sleep(0.2)
    
    def end(self):
        for i in range(self.PIXELS_N):
            self.dev.set_pixel(i,1,1,1,1)
        self.dev.show()


class RMS(object):
    ALARM = False
   # BUFFER_SIZE = 1024
    #s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def __init__(self):
        pass 

    def put(self, data):
        if(int(format(audioop.rms(data, 2)))>=3000):
            self.ALARM = True
            print('ALARM !!! {}'.format(audioop.rms(data, 2)))
            MESSAGE = str('1')
            s.send(MESSAGE)
            data_recv = s.recv(BUFFER_SIZE)
            print " ", data_recv
            time.sleep(3)
        else:
            print('OK -> RMS: {}'.format(audioop.rms(data, 2))) 

 #   def send(self, s):



def main(): 
    rms = RMS()
    pixels = Pixels()

    ####
    TCP_IP = '192.168.1.103'
    #TCP_IP = '127.0.0.1'
    TCP_PORT = 5005
    while True:
        try:
            s.connect((TCP_IP, TCP_PORT))
            print 'Connected'
            break      
        except:
            print 'Failed to connect, will try again in sec'
            time.sleep(1)
    ####
    
    
    audio = Audio(frames_size=1600)
    audio.link(rms)
    audio.start()

    is_quit = []

    def signal_handler(signal, frame):
        print('Quit')
        is_quit.append(True)

    signal.signal(signal.SIGINT, signal_handler)

    while not is_quit:
        try:
            if rms.ALARM:
                pixels.alarm()
            else:
                pixels.cross()
                time.sleep(1) 
                pixels.end() 
                time.sleep(1)   
        except KeyboardInterrupt:
            break
    pixels.end()
    time.sleep(1)
    audio.stop()


if __name__ == '__main__':
    main()
