#!/usr/bin/env python  
import roslib

import rospy
import tf

from sensor_msgs.msg import Imu
from nav_msgs.msg import Odometry
from geometry_msgs.msg import TransformStamped, Quaternion

OdomTopic = rospy.Publisher('/odom', Odometry, queue_size=10)
TransformBroadCaster = tf.TransformBroadcaster()
tf_odom = TransformStamped()
tf_odom.header.frame_id = "odom"
tf_odom.child_frame_id = "base_footprint"
LastImuData = Imu()


def ImuCallback(data):
    global LastImuData
    LastImuData = data
    rospy.loginfo("imu data received")

def OdomCallback(data):
    rospy.loginfo("odom data received")
    time = rospy.Time.now()
    
    angles = tf.transformations.euler_from_quaternion([LastImuData.orientation.x, LastImuData.orientation.y, LastImuData.orientation.z, LastImuData.orientation.w])
    ImuOrientation = tf.transformations.quaternion_from_euler(0,0,angles[2]-3.14/2)
    ImuOrientationQuaternion = Quaternion()
    ImuOrientationQuaternion.x = ImuOrientation[0]
    ImuOrientationQuaternion.y = ImuOrientation[1]
    ImuOrientationQuaternion.z = ImuOrientation[2]
    ImuOrientationQuaternion.w = ImuOrientation[3]
    data.pose.pose.orientation = ImuOrientationQuaternion
    data.twist.twist.angular.z = LastImuData.angular_velocity.z
    data.header.stamp = time

    tf_odom.header.stamp = time
    tf_odom.transform.translation.x = data.pose.pose.position.x
    tf_odom.transform.translation.y = data.pose.pose.position.y

    tf_odom.transform.rotation = ImuOrientationQuaternion
    TransformBroadCaster.sendTransformMessage(tf_odom)

    OdomTopic.publish(data)

def odom_imu_combined():
    rospy.init_node('odom_imu_combined_node', anonymous=False)
    rospy.Subscriber("/odom_simple", Odometry, OdomCallback)
    rospy.Subscriber("/imu/data", Imu, ImuCallback)
    rospy.loginfo("odom_imu_combined started")

    rospy.spin()

if __name__ == '__main__':
    try:
        odom_imu_combined()
    except rospy.ROSInterruptException:
        pass
