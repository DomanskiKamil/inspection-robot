#!/usr/bin/env python
'''imu ROS Node'''
# license removed for brevity
import rospy
import std_msgs
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Vector3
import serial
from std_msgs.msg import Int16

avg_offset = [0,0,0]
calibrate_NOS = 100
calibrate_sample = 0
calibrate_enable = 0
offset_sum = [0, 0, 0]
calibration_delay = 0

def enable_calibration(number_of_samples):
    global calibrate_NOS
    global calibrate_sample
    global offset_sum 
    global calibrate_enable 
    if (calibrate_enable == 0):
        calibrate_NOS = number_of_samples.data
        calibrate_sample = 0
        offset_sum = [0,0,0]
        calibrate_enable = 1
        print "Zadanie kalibracji przyjete", calibrate_NOS
    else:
        print "Odmowa, kalibracja juz trwa"

def calibrate(gx, gy, gz, ax, ay, az, number_of_samples):
    global calibrate_NOS
    global avg_offset
    global calibrate_sample 
    global calibrate_enable 
    global offset_sum
    global calibration_delay 
    a_module = pow((pow(ax,2)+pow(ay,2)+ pow(az,2)),0.5)
    gyro_module = pow((pow(gx,2)+pow(gy,2)+ pow(gz,2)),0.5)
    if ((9.81*0.1 < a_module < 9.81*1.1 ) and (gyro_module < 0.1)):
        if (calibration_delay <= calibrate_sample < (calibrate_NOS + calibration_delay)):
            offset_sum[0] = offset_sum[0] + gx
            offset_sum[1] = offset_sum[1] + gy
            offset_sum[2] = offset_sum[2] + gz
            calibrate_sample = calibrate_sample + 1
            print "Kalibruje", calibrate_sample, calibrate_NOS
        elif(calibrate_sample >= (calibrate_NOS + calibration_delay)):
            avg_offset[0] = offset_sum[0]/calibrate_NOS
            avg_offset[1] = offset_sum[1]/calibrate_NOS
            avg_offset[2] = offset_sum[2]/calibrate_NOS
            print "Wyliczone offsety:", avg_offset
            calibrate_enable = 0
            calibration_delay = 0
            print "Koniec kalibracji"
        else:
            calibrate_sample = calibrate_sample + 1
    else:
        print "Kalibracja przerwana"
        calibrate_sample = 0
        calibration_delay = 10
        offset_sum = [0,0,0]

def talker():
    '''imu Publisher'''
    global avg_offset
    global calibrate_NOS 
    global calibrate_enable 
    
    pub = rospy.Publisher('imu/data_raw', Imu, queue_size=10)
    
    q = Quaternion(0,0,0,0)
    qc = [-1.0, 0, 0, 0, 0, 0, 0, 0, 0]
    avc = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    lac = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    
    #sudo chmod a+rw /dev/ttyACM0
    ser = serial.Serial('/dev/ttyACM0')
    print(ser.name) 
    
    rospy.init_node('imu', anonymous=True)
    # rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        pomiar = ser.readline()
        #print(pomiar)
        pomiar = pomiar.split(" ")
        if len(pomiar) == 6:
            try:
                ax = float(pomiar[0])*9.81
                ay = float(pomiar[1])*9.81
                az = float(pomiar[2])*9.81
                gx = float(pomiar[3])*3.14/180/2 # nie wiem czemu ta dwojka, moze arudino wysyla
                gy = float(pomiar[4])*3.14/180/2 # dwa razy wieksza predkosc
                gz = float(pomiar[5])*3.14/180/2
                #print(ax,ay,az,gx,gy,gz)
                #print(gx,gy,gz)
                h = std_msgs.msg.Header()
                h.stamp = rospy.Time.now()
                h.frame_id = "imu"
                
                
                if (calibrate_enable == 1):
                    calibrate(gx,gy,gz,ax,ay,az,calibrate_NOS)
                
                gx = gx - avg_offset[0]
                gy = gy - avg_offset[1]
                gz = gz - avg_offset[2]
                av = Vector3(gx,gy,gz)
                la = Vector3(ax,ay,az)
                #print (av,la)
                wiad = Imu(h,q,qc,av,avc,la,lac)
                pub.publish(wiad)
            except ValueError:
                print "Blednie przeslane dane"
            
               


if __name__ == '__main__':
    try:
        rospy.Subscriber("/calibrate",Int16 , enable_calibration)
        calibrate_enable = 1
        talker()
    except rospy.ROSInterruptException:
        pass
