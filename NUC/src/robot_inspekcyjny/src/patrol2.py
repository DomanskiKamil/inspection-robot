#!/usr/bin/env python
import sys
import rospy
import Queue
import math
import copy
from visualization_msgs.msg import Marker, MarkerArray

from move_base_msgs.msg import MoveBaseActionResult
from geometry_msgs.msg import PoseStamped, PointStamped, PoseWithCovarianceStamped

from tf.msg import tfMessage
from tf.transformations import quaternion_from_euler


PatrolPoints = Queue.Queue()
GoalTopic = rospy.Publisher('/move_base_simple/goal', PoseStamped, queue_size=10)
MarkersTopic = rospy.Publisher('/patrol_points',MarkerArray, queue_size=10)
markerArray = MarkerArray()
velocity = 4
Patrolling = 0
CurentPose = PoseWithCovarianceStamped()
def AddIntruderToMarkerArray(Point):
    marker = Marker()
    marker.header.frame_id = Point.header.frame_id
    marker.type = marker.TEXT_VIEW_FACING
    marker.action = marker.ADD
    marker.scale.x = 1.0
    marker.scale.y = 1.0
    marker.scale.z = 0.5
    marker.text = "INTRUZ"
    marker.color.a = 1.0
    marker.color.r = 1.0
    marker.color.g = 0.0
    marker.color.b = 0.0
    marker.pose.orientation.w = 1.0
    marker.pose.position.x = Point.point.x
    marker.pose.position.y = Point.point.y 
    marker.pose.position.z = Point.point.z+1
    
    markerArray.markers.append(marker)

    marker2 = copy.deepcopy(marker)
    marker2.type = marker.MESH_RESOURCE
    marker2.mesh_resource="package://robot_inspekcyjny/models/wyk2.stl"
    marker2.scale.x = 0.1
    marker2.scale.y = 0.1
    marker2.scale.z = 0.1
    q = quaternion_from_euler(3.14/2, 0, 0)
    marker2.pose.orientation.x = q[0]
    marker2.pose.orientation.y = q[1]
    marker2.pose.orientation.z = q[2]
    marker2.pose.orientation.w = q[3]
    marker2.pose.position.z = Point.point.z+0.2

    markerArray.markers.append(marker2)
    
    id = 0
    for m in markerArray.markers:
        m.id = id
        id += 1
    # Publish the MarkerArray
    MarkersTopic.publish(markerArray)

def AddPointToMarkerArray(Point):
    marker = Marker()
    marker.header.frame_id = Point.header.frame_id
    marker.type = marker.ARROW
    marker.action = marker.ADD
    marker.scale.x = 0.5
    marker.scale.y = 0.5
    marker.scale.z = 0.5
    marker.color.a = 1.0
    marker.color.r = 0.0
    marker.color.g = 1.0
    marker.color.b = 0.0
    marker.pose.orientation = Point.pose.orientation
    marker.pose.position = Point.pose.position
    
    markerArray.markers.append(marker)

    id = 0
    for m in markerArray.markers:
        m.id = id
        id += 1
    # Publish the MarkerArray
    MarkersTopic.publish(markerArray)

def StartPatrol():
    if not PatrolPoints.empty():
        Patrolling = 1
        GoToNextPoint()

def EndPatrol():
    Patrolling = 0
    rospy.loginfo("Patrol has been completed")

def GoToNextPoint():
    if not PatrolPoints.empty():
        point = PatrolPoints.get()
        PatrolPoints.put(point)
        rospy.loginfo("Next  point x: %f, y:%f",point.pose.position.x,point.pose.position.y)
        rospy.loginfo("curent pose x: %f, y:%f",CurentPose.pose.pose.position.x,CurentPose.pose.pose.position.y)
        #yaw = math.atan2(point.point.y - CurentPose.pose.pose.position.y,point.point.x - CurentPose.pose.pose.position.x)
       # rospy.loginfo(yaw)
        #q = quaternion_from_euler(0, 0, yaw)
        #rospy.loginfo("The quaternion representation is %s %s %s %s.",  q[0], q[1], q[2], q[3])
        #msg = PoseStamped()
        msg.header.frame_id = 'map'
      #  msg.pose.orientation.x = q[0]
       # msg.pose.orientation.y = q[1]
       # msg.pose.orientation.z = q[2]
       # msg.pose.orientation.w = q[3]
        msg.pose.orientation = point.pose.orientation
        msg.pose.position = point.pose.position
        msg.pose.position.x = point.point.x
        msg.pose.position.y = point.point.y
        msg.pose.position.z = point.point.z
        msg.pose.orientation.w = 1
        GoalTopic.publish(msg)
    else:
        EndPatrol()

def RobotResultcallback(data):
    '''xbox_gamepad_velocyty_control Callback Function'''
    if data.status.text == "Goal reached.":
        rospy.loginfo("Goal reached. :)")
        GoToNextPoint()

def PointClickCallback(data):
    rospy.loginfo("Got a point")
    rospy.loginfo("x: %f, y:%f",data.point.x,data.point.y)
    PatrolPoints.put(data)
    AddPointToMarkerArray(data)

def PoseCallback(data):
    CurentPose.pose = data.pose

def listener():
    '''xbox_gamepad_velocyty_control Subscriber'''
   
    rospy.init_node('Patrol', anonymous=True)    
    rospy.Subscriber("move_base/result", MoveBaseActionResult, RobotResultcallback)
    rospy.Subscriber("/move_base_simple/goal2", PoseStamped, PointClickCallback)
    rospy.Subscriber("amcl_pose", PoseWithCovarianceStamped, PoseCallback)
    # spin() simply keeps python from exiting until this node is stopped
    # rospy.spin()
    MarkersTopic.publish(markerArray)
    raw_input("Start Patrol!!:")
    StartPatrol()
    rospy.spin()

if __name__ == '__main__':
    listener()
