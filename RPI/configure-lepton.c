
#include "leptonSDKEmb32PUB/LEPTON_RAD.h"
#include "leptonSDKEmb32PUB/LEPTON_SDK.h"
#include "leptonSDKEmb32PUB/LEPTON_SYS.h"
#include "leptonSDKEmb32PUB/LEPTON_Types.h"

#include <stdio.h>

bool _connected;

LEP_CAMERA_PORT_DESC_T _port;

int lepton_connect() {
	LEP_RESULT result = LEP_OpenPort(1, LEP_CCI_TWI, 400, &_port);
	if (result == LEP_OK){
		_connected = true;
	}else{
		_connected = false;
	}
	return 0;
}


int main(){

	if(!_connected) {
		lepton_connect();
	}
	if (_connected){
		LEP_RAD_ENABLE_E radState=LEP_RAD_DISABLE;
		LEP_RAD_ENABLE_E_PTR radStatePtr;
		radStatePtr = &radState;
		
		//printf("Rozpoczecie konfiguracji...\n");
		LEP_GetRadEnableState(&_port, radStatePtr);
		//printf("Odczytana wartosc RadEnableState: %d\n",*radStatePtr);
		
		radState=LEP_RAD_ENABLE;
		LEP_SetRadEnableState(&_port, radState);
		//printf("Ustawienie RadEnableState na: %d\n",*radStatePtr);
		
		LEP_GetRadEnableState(&_port, radStatePtr);
		//printf("Odczytana wartosc RadEnableState: %d\n",*radStatePtr);
		//printf("Konfiguracja zakonczona \n");
		printf("%d",*radStatePtr);
	}else{
		printf("0");
	}
	return 0;
}
