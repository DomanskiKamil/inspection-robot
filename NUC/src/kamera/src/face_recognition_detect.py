#!/usr/bin/env python
'''kamera ROS Node'''
import roslib
import sys
import rospy
import cv2
import numpy as np
import std_msgs
from std_msgs.msg import String
from kamera.msg import Faces
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from time import sleep



    
    
    

def callback(data):
    '''kamera Callback Function'''
    global frame 
    if (frame == 1):
        start_time = rospy.Time.now()
        bridge = CvBridge()
        try:
            img = bridge.imgmsg_to_cv2(data, "bgr8")
        except CvBridgeError as e:
            print(e)
        
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        
        
        faces_arr = []
        eyes1 = []
        eyes2 = []
        
        faces = face_cascade.detectMultiScale(gray)
        for (x,y,w,h) in faces:
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            
            eyes = eye_cascade.detectMultiScale(roi_gray)
            i = 1
            for (ex,ey,ew,eh) in eyes:
                
                if (i == 1):
                    ex1 = ex
                    ey1 = ey
                    ew1 = ew
                    eh1 = eh
                elif (i == 2):
                    eyes1.append(ex1)
                    eyes1.append(ey1)
                    eyes1.append(ew1)
                    eyes1.append(eh1)
                    eyes2.append(ex)
                    eyes2.append(ey)
                    eyes2.append(ew)
                    eyes2.append(eh)
                    faces_arr.append(x)
                    faces_arr.append(y)
                    faces_arr.append(w)
                    faces_arr.append(h)
                    rospy.loginfo("Face detected, delay: %s ms", ((rospy.Time.now() - start_time).nsecs)/(1000000))
                    
                    
                i = i + 1
        
        #talker
        h = std_msgs.msg.Header()
        h.stamp = rospy.Time.now()
        wiad = Faces(h,faces_arr,eyes1,eyes2)
        pub.publish(wiad)    
    frame = frame + 1
    if (frame == 5):
        frame = 1
    

    
    

def listener():
    '''kamera Subscriber'''
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('kamera', anonymous=True)

    global face_cascade
    global eye_cascade
    
    global frame 
    frame = 1
    

    face_cascade = cv2.CascadeClassifier('/opt/ros/kinetic/share/OpenCV-3.3.1-dev/haarcascades/haarcascade_frontalface_default.xml')
    eye_cascade = cv2.CascadeClassifier('/opt/ros/kinetic/share/OpenCV-3.3.1-dev/haarcascades/haarcascade_eye.xml')


    rospy.Subscriber("/cv_camera/image_raw", Image, callback)

    #initialize talker
    global pub
    pub = rospy.Publisher('rectangles',Faces, queue_size=10)
    rospy.init_node('kamera', anonymous=True)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
