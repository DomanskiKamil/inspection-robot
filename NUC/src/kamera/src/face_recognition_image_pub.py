#!/usr/bin/env python
'''kamera ROS Node'''
import roslib
import sys
import rospy
import cv2
import numpy as np
import message_filters
from kamera.msg import Faces
from sensor_msgs.msg import CompressedImage
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

    
def callback1(image_data):
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", faces_data)
    #rospy.loginfo(rospy.get_caller_id() + "I heard %s", image_data.height)
    bridge = CvBridge()
    try:
        img = bridge.imgmsg_to_cv2(image_data, "bgr8")
    except CvBridgeError as e:
        print(e)
    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    if (global_faces_data != []):
        faces = global_faces_data.faces
        eyes1 = global_faces_data.eyes1
        eyes2 = global_faces_data.eyes2

        for i in range(0,(len(faces)-1),4):
            x = faces[i]
            y = faces[i+1]
            w = faces[i+2]
            h = faces[i+3]
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            
            ex = eyes1[i]
            ey = eyes1[i+1]
            ew = eyes1[i+2]
            eh = eyes1[i+3]
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)
        
            ex = eyes2[i]
            ey = eyes2[i+1]
            ew = eyes2[i+2]
            eh = eyes2[i+3]
            cv2.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),2)  
       
    pub.publish(bridge.cv2_to_imgmsg(img,image_data.encoding))
    #pub.publish(bridge.cv2_to_compressed_imgmsg(img))

def callback2(faces_data):
    global global_faces_data
    global_faces_data = faces_data

def listener():
    '''kamera Subscriber'''
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('kamera', anonymous=True)
    global pub
    pub = rospy.Publisher('image_face_detected',Image, queue_size=10)

    global global_faces_data
    global_faces_data = []

    rospy.Subscriber("/cv_camera/image_raw", Image, callback1)
    rospy.Subscriber("/rectangles", Faces, callback2)

    #image_sub = message_filters.Subscriber("/cv_camera/image_raw", Image)
    #faces_sub = message_filters.Subscriber("/rectangles", Faces)
    
    #ts = message_filters.ApproximateTimeSynchronizer([image_sub, faces_sub], 10,1)
    #ts.registerCallback(callback)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()
