#!/usr/bin/env python  
import roslib

import rospy
import tf

if __name__ == '__main__':
    rospy.init_node('laser_tf_broadcaster')
    br = tf.TransformBroadcaster()
    rate = rospy.Rate(10.0)
    while not rospy.is_shutdown():
        br.sendTransform((0.0, 0.0, 0.2),
                         tf.transformations.quaternion_from_euler(0,0, 3.14),
                         rospy.Time.now(),
                         "laser",
                         "base_link")
        rate.sleep()
